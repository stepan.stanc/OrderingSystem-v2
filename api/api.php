<?php
include "pdo.php";
       
$action = "";
$data = "";
//json_encode()
//json_decode()

/*
$request = json_decode($data);
echo $request->{'id'}; //pro data=[{"id":"1"}]
*/

//user , product , shopping_cart (vazební)
//SELECT * FROM product JOIN [order_products] ON [order_products].[order_id] = $data WHERE [order_products].[product_id] = [product].[id]

switch ($_SERVER["REQUEST_METHOD"]) {
  case "GET": header ("Content-Type: text/json");
  {
    if(isset($_GET["action"])) { $action =  $_GET["action"]; }
    if(isset($_GET["data"])) { $data =  json_decode($_GET["data"]); }
    
    
    switch ($action) {

      case 'get-all-usrs':
      {
        echo QuerySql("SELECT * FROM user");
        break; 
      }
      case 'get-all-prods':
      {
        echo QuerySql("SELECT * FROM product");
        break; 
      }
      case 'get-prod':
      {
        echo QuerySql("SELECT * FROM product WHERE id = $data");
        break; 
      }
      case 'get-usr':
      {
        echo QuerySql("SELECT * FROM user WHERE id = $data");
        break; 
      }          
      case 'get-usr-cart':
      {
        echo QuerySql("SELECT * FROM  `order` WHERE user_id = $data AND ordered = FALSE");
        break; 
      }   
      case 'get-order':
      {
        echo QuerySql("SELECT * FROM  `order` WHERE id = $data");
        break; 
      }   
      case 'get-usr-orders':
      {
        echo QuerySql("SELECT * FROM  `order` WHERE user_id = $data");
        break; 
      }
      case 'get-order-products':
      {
        echo QuerySql("SELECT product.id, product.name, product.price, product.description, order_products.count FROM product LEFT JOIN order_products ON order_products.order_id = $data WHERE order_products.product_id = product.id");
        break; 
      }
      default:
      {
        echo "unknown request";

        break;
      }

    }

    break;
  }
  
  case 'POST': header ("Content-Type: text/json");
  {
    if(isset($_POST["action"])) { $action =  $_POST["action"]; }
    if(isset($_POST["data"])) { $data =  json_decode($_POST["data"]); }
    $name = NULL;
    $surname = Null;
    $birth = NULL;
    $count = NULL;
    $description = "";
    $ordered = false;
    $hidden = false;

    switch ($action) {      


      case 'log-in':
      {               
        $name = $data->{"username"};
        $pass = $data->{"password"};        
        echo QuerySql("SELECT * FROM user WHERE username = '$name' AND password = '$pass'");
        break; 
      }      
      case 'user-by-name':
      {               
        $name = $data->{"username"};        
        echo QuerySql("SELECT * FROM user WHERE username = '$name'");
        break; 
      }        
      case 'save-prod':
      {       
        foreach ($data as $value) {
          $name = $value->{"name"};
          $price = $value->{"price"};
          if(isset($value->{"description"})){$description = $value->{"description"};}
          QuerySql("INSERT INTO product (name, price, description) VALUES ('$name', $price,'$description')");       
        }                                 
        break; 
      }        
      case 'save-usr':
      {
        foreach ($data as $value) {
          if(isset($value->{"name"})){$name = $value->{"name"};}
          if(isset($value->{"surname"})){$surname = $value->{"surname"};}
          $username = $value->{"username"};
          if(isset($value->{"birthdate"})){$birth = $value->{"birthdate"};}
          $pass = $value->{"password"};
          echo QuerySql("INSERT INTO user(name, surname, username, birthdate, password) VALUES ('$name','$surname','$username','$birth','$pass')");
        }
        break; 
      }      
      case 'save-order' : 
      {
        foreach ($data as $value) {
          $user_id = $value->{"user_id"};
          $datetime = $value->{"datetime"};
          if(isset($value->{"ordered"})){$ordered = $value->{"ordered"};}
          if(isset($value->{"hidden"})){$hidden = $value->{"hidden"};}
          QuerySql("INSERT INTO `order`(user_id, datetime, ordered, hidden) VALUES ($user_id,'$datetime','$ordered','$hidden')");
        }
        break;
      }
      case 'save-order-products' : 
      {
        foreach ($data as $value) {
          $order_id = $value->{"order_id"};
          $product_id = $value->{"product_id"};
          if(isset($value->{"count"})){$count = $value->{"count"};}
          QuerySql("INSERT INTO order_products(product_id,order_id,count) VALUES ($product_id,$order_id,$count)");
        }
        break;
      }
      case 'update-prod':
      {     
        foreach ($data as $value) {  
          $id = $value->{"id"};         
          $name = $value->{"name"};
          $price = $value->{"price"};
          if(isset($value->{"description"})){$description = $value->{"description"};}
          QuerySql("UPDATE product SET name='$name',price=$price,description='$description' WHERE id = $id");
        }
        break; 
      }        
      case 'update-usr':
      {
        foreach ($data as $value) {
          $id = $value->{"id"};
          if(isset($value->{"name"})){$name = $value->{"name"};}
          if(isset($value->{"surname"})){$surname = $value->{"surname"};}
          $username = $value->{"username"};
          if(isset($value->{"birthdate"})){$birth = $value->{"birthdate"};}
          $pass = $value->{"password"};
          QuerySql("UPDATE user SET name='$name',surname='$surname',username='$username',birthdate='$birth',password='$pass' WHERE id = $id");
        }
        break; 
      }   
      case 'update-order' : 
      {
        foreach ($data as $value) {
          $id = $value->{"id"};
          $user_id = $value->{"user_id"};
          $datetime = $value->{"datetime"};
          if(isset($value->{"ordered"})){$ordered = $value->{"ordered"};}
          if(isset($value->{"hidden"})){$hidden = $value->{"hidden"};}
          QuerySql("UPDATE `order` SET user_id=$user_id,datetime='$datetime',ordered='$ordered',hidden='$hidden' WHERE `order`.id = $id");
        }
        break;
      }
      case 'update-order-products' : 
      {
        foreach ($data as $value) {
          $id = $value->{"id"};
          $order_id = $value->{"order_id"};
          $product_id = $value->{"product_id"};
          if(isset($value->{"count"})){$count = $value->{"count"};}
          QuerySql("UPDATE order_products SET order_id=$order_id,product_id=$product_id,count=$count WHERE id = $id");
        }
        break;
      }   
      case 'delete-usr':
      {
        foreach ($data as $id) {  
          QuerySql("DELETE FROM shopping_cart WHERE user_id = $id");
          QuerySql("DELETE FROM user WHERE id = $id");
        }
        
        break;
      }
      case 'delete-prod':
      {
        foreach ($data as $id) {  
          QuerySql("DELETE FROM shopping_cart WHERE product_id = $id");
          QuerySql("DELETE FROM product WHERE id = $id");
        }
        break;
      }
      case 'delete-order' :
      {
        foreach ($data as $id) {  
          QuerySql("DELETE FROM order_products WHERE order_id = $id");
          QuerySql("DELETE FROM `order` WHERE id = $id");
        }
        break;
      }
      case 'delete-order-products' :
      {
        foreach ($data as $id) {  
          QuerySql("DELETE FROM order_products WHERE order_id = $id");
        }
        break;
      }
      default:
      {
        echo "unknown request";

        break;
      }
      
    }

    break;
  }  
  default:
  {
    echo "request is not supported";

    break;
  }

}

function QuerySql($query){  
  $result = $GLOBALS['db']->prepare($query); 
  $result->execute();
  return json_encode ($result->fetchAll());
}

?>