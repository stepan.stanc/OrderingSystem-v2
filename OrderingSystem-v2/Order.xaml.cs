using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OrderingSystem_v2.Models;
using OrderingSystem_v2.WebClient;
using OrderingSystem_v2.Utils;

namespace OrderingSystem_v2
{
  /// <summary>
  /// Interaction logic for Order.xaml
  /// </summary>
  public partial class Order : Page
  {
    int order_id = 0;
    ViewOrder order;
    int user_id = 1;
    public Rest api = new Rest();
    public Sync sync = new Sync();
    List<ViewProduct> orderProducts = new List<ViewProduct>();
    List<product> products = new List<product>();
    public Order(ViewOrder ordr,int usr)
    {
      InitializeComponent();
      loadProducts();
      loadOrderProds();

      order_id = ordr.id;
      order = ordr;
      user_id = usr;
      save.Content = ordr.ordered ? "back" : "save";      

      RefreshAllProds();
      foreach (ViewProduct prod in orderProducts)
      {
        orderProds.Items.Add(prod);
        //allProds.Items.Remove(allProds.Items.OfType<product>().ToList().Where(pr => pr.id == prod.id).First());
      }
      allProds.Items.Refresh();
      orderProds.Items.Refresh();
    }

    private async void loadProducts()
    {
      products = await sync.GetAllProducts();
      RefreshAllProds();
    }

    private async void loadOrderProds()
    {
      orderProducts = await sync.GetOrderProducts(order_id);
    }

    private void allSelect(object sender, RoutedEventArgs e)
    {
      var selected = allProds.SelectedItem as product;
      if (selected == null || order.ordered) return;
      var contains = orderProds.Items.Cast<ViewProduct>().ToList().Find(x => x.id == selected.id);
      if(contains == null)
      {
        orderProds.Items.Add(new ViewProduct { id = selected.id, description = selected.description, name = selected.name, price = selected.price });
      }
      else
      {
        contains.count++;
        orderProds.Items.Remove(contains);
        orderProds.Items.Add(contains);
      }
    }

    private void ordrSelect(object sender, RoutedEventArgs e)
    {
      var selected = orderProds.SelectedItem as ViewProduct;
      if (selected == null || order.ordered) return;
      if(selected.count <= 1)
      {
        orderProds.Items.Remove(selected);
      }
      else
      {
        selected.count--;
        orderProds.Items.Remove(selected);
        orderProds.Items.Add(selected);
      }
    }

    public void RefreshAllProds()
    {
      loadProducts();
      allProds.Items.Clear();
      
      foreach (product prod in products)
      {
        allProds.Items.Add(prod);
      
      }
      allProds.Items.Refresh();
      
    }

    private void save_Click(object sender, RoutedEventArgs e)
    {
      if(order.ordered) App.Current.MainWindow.Content = new MainPage(user_id);
      
      var o_p = new List<order_products>();
      foreach (ViewProduct prod in orderProds.Items)
      {
        o_p.Add(new order_products { order_id = order_id, product_id = prod.id, count = prod.count });
      }
      sync.SaveOrderProds(o_p, order_id);
      App.Current.MainWindow.Content = new MainPage(user_id);
      orderProds.Items.Clear();
      allProds.Items.Clear();
    }
  }
}
