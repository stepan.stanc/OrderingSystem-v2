using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderingSystem_v2.WebClient;
using OrderingSystem_v2.Database;
using OrderingSystem_v2.Models;
using System.Net;
using OrderingSystem_v2.Utils;
using System.Runtime;
using System.Runtime.InteropServices;

namespace OrderingSystem_v2.WebClient
{
  public class Sync
  {
    /*
    ins - insert
    upd - update

    on - online
    of - ofline

    dt - DateTime
    */
    //public bool InternetAvailability.IsInternetAvailable() = true;

    #region init
    public Rest rest = new Rest();
    public int user_id = 0;

    public Sync()
    {
      //ApiInternetAvailability.IsInternetAvailable()();
    }

    public Sync(int usr_id)
    {
      user_id = usr_id;
      //ApiInternetAvailability.IsInternetAvailable()();
    }

    private static ItemDatabase _database;
    //instance databáze
    public static ItemDatabase Database
    {
      get
      {
        if (_database == null)
        {
          var fileHelper = new FileHelper();
          _database = new ItemDatabase(fileHelper.GetLocalFilePath("SQLite.db3"));
        }
        return _database;
      }
    }
    #endregion

    #region sync methods
    public void SyncAll()
    {
      if (user_id == 0) return;
      //ApiInternetAvailability.IsInternetAvailable()();
      if (InternetAvailability.IsInternetAvailable())
      {

        Database.ClearDB();


        List<user> OnUsrs = rest.GetUsers();

        List<product> OnProds = rest.GetAllProducts();

        List<order> OnUsrOrds = rest.GetUserOrders(user_id);

        List<order_products> OnOrdProds = new List<order_products>();
        foreach(order ordr in OnUsrOrds)
        {
          List<ViewProduct> vp = rest.GetOrderProducts(ordr.id);
          foreach (ViewProduct prod in vp)
          {
            OnOrdProds.Add(new order_products { order_id = ordr.id, product_id = prod.id, count = prod.count });
          }
        }

        Database.SaveItemsAsync<user>( OnUsrs , true);

        Database.SaveItemsAsync<product>(OnProds,true);

        Database.SaveItemsAsync<order>(OnUsrOrds, true);

        Database.SaveItemsAsync<order_products>(OnOrdProds, true);

      }
    }

    /*
    /// <summary>
    /// porovná data z webu a z lokálního úložiště a podle posledních úprav data zpracuje
    /// </summary>
    /// <typeparam name="T">class representation of table</typeparam>
    /// <param name="onTable">online table</param>
    /// <param name="ofTable">ofline table</param>
    private async void SolveConflict<T>(List<T> onTable, List<T> ofTable) where T : ITable, new()
    {
      if (onTable == ofTable)
      {
        return;
      }
      else
      {

        foreach (T onItem in onTable)
        {
          if (await Database.IsDeleted<T>(onItem.id)) //check of deleted
          {
            switch (typeof(T).Name)
            {
              case "order":
                rest.DeleteOrder(new List<int> { onItem.id });
                break;
              case "product":
                rest.DeleteProduct(new List<int> { onItem.id });
                break;
              default:
                break;
            }
          }
          else
          {
            T ofItem = new T();

            try
            {
              ofItem = ofTable.Find(i => i.id == onItem.id);
              ofTable.Remove(ofItem);

              if ((ITable)ofItem == (ITable)onItem) return;

              DateTime ofUp = ofItem == null || string.IsNullOrWhiteSpace(ofItem.timestamp_upd) ? new DateTime() : ofItem.timestamp_upd.FromPHPDatetime();
              DateTime ofIn = ofItem == null || string.IsNullOrWhiteSpace(ofItem.timestamp_ins) ? new DateTime() : ofItem.timestamp_ins.FromPHPDatetime();
              DateTime onUp = string.IsNullOrWhiteSpace(onItem.timestamp_upd) ? new DateTime() : onItem.timestamp_upd.FromPHPDatetime();
              DateTime onIn = string.IsNullOrWhiteSpace(onItem.timestamp_ins) ? new DateTime() : onItem.timestamp_ins.FromPHPDatetime();

              DateTime ofDt = ofIn >= ofUp ? ofIn : ofUp;
              DateTime onDt = onIn >= onUp ? onIn : onUp;

              if (ofDt >= onDt)
              {
                //rest
                if (ofIn >= ofUp)
                {
                  //in
                  switch (typeof(T).Name)
                  {
                    case "order":
                      rest.SaveOrder(new List<order> { onItem as order });
                      break;
                    case "product":
                      rest.SaveProduct(new List<product> { onItem as product });
                      break;
                    case "user":
                      rest.SaveUser(new List<user> { onItem as user });
                      break;
                    case "order_products":
                      rest.SaveOrderProd(onItem as order_products);
                      break;
                    default:
                      break;
                  }
                }
                else
                {
                  //up
                  switch (typeof(T).Name)
                  {
                    case "order":
                      rest.UpdateOrder(new List<order> { onItem as order });
                      break;
                    case "user":
                      rest.UpdateUser(new List<user> { onItem as user });
                      break;
                    default:
                      break;
                  }
                }
              }
              else
              {
                //database
                if (onIn >= onUp)
                {
                  //in
                  switch (typeof(T).Name)
                  {
                    case "order":
                      await Database.SaveItemAsync(onItem as order, true);
                      break;
                    case "product":
                      await Database.SaveItemAsync(onItem as product, true);
                      break;
                    case "user":
                      await Database.SaveItemAsync(onItem as user, true);
                      break;
                    case "order_products":
                      await Database.SaveItemAsync(onItem as order_products, true);
                      break;
                    default:
                      break;
                  }
                }
                else
                {
                  //up
                  switch (typeof(T).Name)
                  {
                    case "order":
                      await Database.SaveItemAsync(onItem as order, false);
                      break;
                    case "user":
                      await Database.SaveItemAsync(onItem as user, false);
                      break;
                    default:
                      break;
                  }
                }
              }

            }
            catch
            {
              switch (typeof(T).Name)
              {
                case "order":
                  await Database.SaveItemAsync(onItem as order, true);
                  break;
                case "product":
                  await Database.SaveItemAsync(onItem as product, true);
                  break;
                case "user":
                  await Database.SaveItemAsync(onItem as user, true);
                  break;
                case "order_products":
                  await Database.SaveItemAsync(onItem as order_products, true);
                  break;
                default:
                  break;
              }
            }
          }

            

          if (ofTable.Count > 0)
          {
            //rest - in
            switch (typeof(T).Name)
            {
              case "order":
                rest.SaveOrder(new List<order> { onItem as order });
                break;
              case "product":
                rest.SaveProduct(new List<product> { onItem as product });
                break;
              case "user":
                rest.SaveUser(new List<user> { onItem as user });
                break;
              case "order_products":
                rest.SaveOrderProd(onItem as order_products);
                break;
              default:
                break;
            }
          }

        }
      }
    }

    

      switch (typeof(T).Name)
          {
            case "order":
              break;
            case "product":
              break;
            case "user":
              break;
            case "order_products":
              break;
            default:
              break;
          }
       */

    #endregion

    #region api + db methods
    /*
  public async Task<List<user>> Login(user usr)
  {
    if (InternetAvailability.IsInternetAvailable())
    {
      return rest.Login(usr);

    }
    else
    {
      //db
      try
      {
        var pom = await Database.Login(usr);
        return pom;
      }
      catch
      {
        return null;
      }

    }
  }

  public Task<user> GetUser(int id)
  {
    if (InternetAvailability.IsInternetAvailable())
    {
      //rest
      return Task<user>.Run(() => rest.GetUser(id));
    }
    else
    {
      //db
      var pom = (Task<user>)Database.GetItemAsync<user>(id);
      return pom;
    }
  }

  public void SaveUser(List<user> usr)
  {
    if (InternetAvailability.IsInternetAvailable())
    {
      //rest
      rest.SaveUser(usr);
    }
    else
    {
      //db        
      //Database.SaveItemsAsync<user>(usr.Cast<ITable>().ToList().TimeStampIns().Cast<user>().ToList(), true);
    }
  }

  public void UpdateUser(List<user> usr)
  {
    if (InternetAvailability.IsInternetAvailable())
    {
      //rest
      rest.UpdateUser(usr);
    }
    else
    {
      //db
     // Database.SaveItemsAsync<user>(usr.Cast<ITable>().ToList().TimeStampUpd().Cast<user>().ToList(), false);
    }
  }

  public async Task<List<order>> GetUserOrders(int id_user)
  {
    if (InternetAvailability.IsInternetAvailable())
    {
      //rest
      return rest.GetUserOrders(id_user);
    }
    else
    {
      //db
      var pom = await Database.UsrOrders(id_user); 
      return pom;
    }
  }

  public void SaveOrder(List<order> orders)
  {
    if (InternetAvailability.IsInternetAvailable())
    {
      //rest
      rest.SaveOrder(orders);
    }
    else
    {
      //db
     // Database.SaveItemsAsync<order>(orders.Cast<ITable>().ToList().TimeStampIns().Cast<order>().ToList(), true);
    }
  }

  public void UpdateOrder(List<order> orders)
  {
    if (InternetAvailability.IsInternetAvailable())
    {
      //rest
      rest.UpdateOrder(orders);
    }
    else
    {
      //db
    //  Database.SaveItemsAsync<order>(orders.Cast<ITable>().ToList().TimeStampUpd().Cast<order>().ToList(), false);
    }
  }

  public async Task<List<product>> GetAllProducts()
  {
    if (InternetAvailability.IsInternetAvailable())
    {
      //rest
      return rest.GetAllProducts();
    }
    else
    {
      //db
      var pom = await Database.GetItemsAsync<product>();
      return pom;
    }
  }

  public void DeleteOrder(List<int> ids)
  {
    if (InternetAvailability.IsInternetAvailable())
    {
      //rest
      rest.DeleteOrder(ids);
    }
    else
    {
      //db
     // Database.DeleteItemsAsyncByID<order>(ids);
     // Database.DeleteOrderProducts(ids);
    }
  }

  public void SaveProduct(List<product> prods)
  {
    if (InternetAvailability.IsInternetAvailable())
    {
      //rest
      rest.SaveProduct(prods);
    }
    else
    {
      //db
     // Database.SaveItemsAsync<product>(prods.Cast<ITable>().ToList().TimeStampIns().Cast<product>().ToList(), true);
    }
  }

  public void DeleteProduct(List<int> ids)
  {
    if (InternetAvailability.IsInternetAvailable())
    {
      //rest
      rest.DeleteProduct(ids);
    }
    else
    {
      //db
      //Database.DeleteItemsAsyncByID<product>(ids);
      //Database.DeleteProductOrderProducts(ids);
    }
  }

  public async Task<List<ViewProduct>> GetOrderProducts(int id)
  {
    if (InternetAvailability.IsInternetAvailable())
    {
      //rest
      return rest.GetOrderProducts(id);
    }
    else
    {
      //db
      var pom = await Database.GetOrderProducts(id);
      return pom;
    }
  }

  public void SaveOrderProds(List<order_products> prods, int order_id)
  {
    if (InternetAvailability.IsInternetAvailable())
    {
      //rest
      rest.SaveOrderProds(prods, order_id);
    }
    else
    {
      //db
      //Database.DeleteOrderProducts(new List<int> { order_id });
      //Database.SaveItemsAsync<order_products>(prods.Cast<ITable>().ToList().TimeStampIns().Cast<order_products>().ToList(), true);
    }
  }

  /*
  if (InternetAvailability.IsInternetAvailable())
  {
    //rest

  }
  else
  {
    //db

  }
    */
    #endregion

    #region InternetAvailability.IsInternetAvailable() checks
    /*
  public void Aconnection()
  {
    InternetAvailability.IsInternetAvailable() = CheckInternetAvailability.IsInternetAvailable()("https://student.sps-prosek.cz/~stancst14/api_ordering_system");
  }

  public bool CheckInternetAvailability.IsInternetAvailable()(String URL)
  {
    try
    {
      HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
      request.Timeout = 4000;
      request.Credentials = CredentialCache.DefaultNetworkCredentials;
      HttpWebResponse response = (HttpWebResponse)request.GetResponse();

      if (response.StatusCode == HttpStatusCode.OK)
        return true;
      else
        return false;
    }
    catch
    {
      return false;
    }
  }

  public static bool IsconnectionAvailable()
  {
    System.Net.WebRequest req = System.Net.WebRequest.Create("https://www.google.co.in/");
    System.Net.WebResponse resp;
    try
    {
      resp = req.GetResponse();
      resp.Close();
      req = null;
      return true;
    }
    catch (Exception ex)
    {
      req = null;
      return false;
    }
  }
  */
    #endregion
  }
  public static class InternetAvailability
  {
    [DllImport("wininet.dll")]
    private extern static bool InternetGetConnectedState(out int description, int reservedValue);

    public static bool IsInternetAvailable()
    {
      int description;
      return InternetGetConnectedState(out description, 0);
    }
  }
}
