using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderingSystem_v2.Database;
using SQLite;

namespace OrderingSystem_v2.Models
{
  public class deleted : ITable
  {
    [PrimaryKey, AutoIncrement]
    public int id { get; set; }
    public int item_id { get; set; }
    public string table_name { get; set; }
    public string timestamp { get; set; }

    public string timestamp_ins { get; set; }
    public string timestamp_upd { get; set; }

  }
}
