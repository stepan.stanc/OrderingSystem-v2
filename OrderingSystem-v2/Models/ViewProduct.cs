using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderingSystem_v2.Database;
using SQLite;

namespace OrderingSystem_v2.Models
{
    public class ViewProduct : product
    {
        public int count { get; set; } = 1;
        public long fullPrice { get { return (long)price * count; } }
    }
}
