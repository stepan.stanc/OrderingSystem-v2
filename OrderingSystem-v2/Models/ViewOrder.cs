using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderingSystem_v2.WebClient;
using OrderingSystem_v2.Database;
using SQLite;

namespace OrderingSystem_v2.Models
{
    public class ViewOrder : order
    {
        private Rest api = new Rest();
        private long prc = 0;

        public string products
        {
            get
            {
                var prods = api.GetOrderProducts(id);
                var str = "";
                foreach (ViewProduct prod in prods)
                {
                    str += prod.count + "x " + prod.name + ", ";
                    prc += prod.fullPrice;
                }
                return str;
            }
        }
        public long fullPrice
        {
            get
            {
                return prc;
            }
        }

        public override string ToString()
        {
            return "Order date : " + datetime.ToString();
        }
    }
}
