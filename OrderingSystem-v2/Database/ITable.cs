using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using OrderingSystem_v2.Utils;

namespace OrderingSystem_v2.Database
{
  public interface ITable
  {
    [PrimaryKey]
    int id { get; set; }
    string timestamp_ins { get; set; } 
    string timestamp_upd { get; set; } 
  }

  public static class ITableUtils
  {
    public static ITable NullId(this ITable item)
    {
      item.id = 0;
      return item;
    }

    public static List<ITable> TimeStampIns(this List<ITable> items)
    {
      List<ITable> list = new List<ITable>();
      foreach(ITable item in items)
      {
        list.Add(item.TimeStampIn());
      }

      return list;
    }

    public static List<ITable> TimeStampUpd(this List<ITable> items)
    {
      List<ITable> list = new List<ITable>();
      foreach (ITable item in items)
      {        
        list.Add(item.TimeStampUp());
      }

      return list;
    }

    public static ITable TimeStampUp(this ITable item)
    {
      item.timestamp_upd = DateTime.Now.ToPHPDatetime(); 
      return item;
    }
    public static ITable TimeStampIn(this ITable item)
    {
      item.timestamp_ins = DateTime.Now.ToPHPDatetime();
      return item;
    }
  }
}
