using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
using OrderingSystem_v2.Models;
using OrderingSystem_v2.Utils;

namespace OrderingSystem_v2.Database
{
  public class ItemDatabase
  {
    // SQLite connection
    private SQLiteAsyncConnection database;

    public ItemDatabase(string dbPath)
    {
      database = new SQLiteAsyncConnection(dbPath);

      database.CreateTableAsync<order>();
      database.CreateTableAsync<order_products>();
      database.CreateTableAsync<product>();
      database.CreateTableAsync<user>();
      database.CreateTableAsync<deleted>();

    }

    // Query
    public Task<List<T>> GetItemsAsync<T>() where T : ITable, new()
    {
       return database.Table<T>().ToListAsync();
    }

    // sql dotaz iniverzální v typu výsledku a v hodnotě i sloupci
    public Task<List<T>> GetItemsByColumnValue<T>(int id, string tableRow) where T : ITable, new()
    {
      string pom = String.Format("SELECT * FROM {2} WHERE {1} = {0}", id, tableRow, typeof(T).Name);
      return database.QueryAsync<T>(pom);
    }

    /// <summary>
    /// Získá poslední použité ID léku
    /// </summary>
    /// <returns></returns>
    public Task<List<T>> GetLastID<T>() where T : ITable, new()
    {
      string pom = String.Format("SELECT * FROM {0} WHERE ID = (SELECT MAX(ID) FROM {0})", typeof(T).Name);
      return database.QueryAsync<T>(pom);
    }
    //SELECT [seq] FROM [sqlite_sequence] WHERE [name] = 'Medication'

    // Query using LINQ
    public Task<T> GetItemAsync<T>(int id) where T : ITable, new()
    {
      return database.Table<T>().Where(i => ((ITable)i).id == id).FirstOrDefaultAsync();
    }

    public Task<int> SaveItemAsync<T>(T item, bool saveOnly) where T : ITable, new()
    {

      if (item.id == 0 || saveOnly)
      {        
        return database.InsertAsync(saveOnly ? item : item.TimeStampIn());
      }
      else
      {
        return database.UpdateAsync(item.TimeStampUp());
      }
    }

    public async void SaveItemsAsync<T>(List<T> items, bool saveOnly) where T : ITable, new()
    {
      foreach(T item in items)
      {
        await SaveItemAsync<T>(item,saveOnly);
      }
    }


    public Task<int> DeleteItemAsync<T>(T item) where T : ITable, new()
    {
      LogDelete(typeof(T).Name, item.id);
      return database.DeleteAsync(item);
    }

    public async void DeleteItemsAsync<T>(List<T> items) where T : ITable, new()
    {
      foreach(T item in items)
      {
        await database.DeleteAsync(item);
        LogDelete(typeof(T).Name, item.id);
      }       
    }

    public Task<List<int>> DeleteItemAsyncByID<T>(int id) where T : ITable, new()
    {
      string pom = String.Format("DELETE FROM {0} WHERE ID = {1}", typeof(T).Name, id);
      LogDelete(typeof(T).Name, id);
      return database.QueryAsync<int>(pom);
    }

    public async void DeleteItemsAsyncByID<T>(List<int> ids) where T : ITable, new()
    {
      foreach(int id in ids)
      {
        await DeleteItemAsyncByID<T>(id);        
      }
    }

    public async void LogDelete(string table,int id)
    {
      string pom = String.Format("INSERT INTO deleted (item_id, table_name, timestamp) VALUES ({0}, '{1}', '{2}')", id, table,DateTime.Now.ToPHPDatetime());
      await database.QueryAsync<deleted>(pom);
    }

    public async Task<bool> IsDeleted<T>(int id) where T : ITable, new()
    {
      try
      {
        var del = new deleted();
        del = await database.Table<deleted>().Where(i => i.id == id && i.table_name == typeof(T).Name).FirstOrDefaultAsync();
        return del != null && del.id == id;
      }
      catch
      {
        return false;
      }
    }

    public async Task<List<user>> Login(user usr)
    {
      return await database.Table<user>().Where(u => u.name == usr.name && u.password == usr.password).ToListAsync();
    }

    public async Task<List<order>> UsrOrders(int usr_id)
    {
      return await database.Table<order>().Where(o => o.user_id == usr_id).ToListAsync();
    }

    public async void DeleteOrderProducts(List<int> ids) 
    {
      foreach(int id in ids)
      {
        string query = String.Format("DELETE FROM order_products WHERE order_id = {0}", id);
        await database.QueryAsync<int>(query);
      }
    }

    public async void DeleteProductOrderProducts(List<int> ids)
    {
      foreach (int id in ids)
      {
        string query = String.Format("DELETE FROM order_products WHERE product_id = {0}", id);
        await database.QueryAsync<int>(query);
      }
    }

    public async Task<List<ViewProduct>> GetOrderProducts(int id)
    {
      string query = String.Format("SELECT product.id, product.name, product.price, product.description, order_products.count FROM product LEFT JOIN order_products ON order_products.order_id = {0} WHERE order_products.product_id = product.id", id);
      return await database.QueryAsync<ViewProduct>(query);
    }

    public async void ClearDB()
    {
      await database.QueryAsync<int>("DELETE FROM 'order'");
      await database.QueryAsync<int>("DELETE FROM order_products");
      await database.QueryAsync<int>("DELETE FROM product");
      await database.QueryAsync<int>("DELETE FROM 'user'");
    }


  }
}
