using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OrderingSystem_v2.Models;
using OrderingSystem_v2.WebClient;
using OrderingSystem_v2.Utils;

namespace OrderingSystem_v2
{
  /// <summary>
  /// Interaction logic for MainPage.xaml
  /// </summary>
  public partial class MainPage : Page
  {

    public Rest api = new Rest();
    public Sync sync;
    int user_id = 1;
    bool showHidden = false;
    user currentUser = new user();
    List<product> products = new List<product>();
    List<order> orders = new List<order>();

    public MainPage(int usr_id)
    {
      InitializeComponent();
      user_id = usr_id;
      sync = new Sync(usr_id);
      //sync.SyncAll();
      
      myLabel.Content = "Current user: " + currentUser.username;

      refershAll();

      //sync.UpdateUser(new List<user> { new user { id = 1, username = "tstaaa", password = "pass", birthdate = user.ToBirthdayFormat(new DateTime(2,2,5)) } } );
      //sync.Saveuser(new List<user> { new user { username = "tstaaa", password = "pass" } });
      //sync.DeleteUser(new List<int> { 20,19 });
    }

    private async Task loadUser()
    {
      currentUser = await sync.GetUser(user_id);
    }

    private async Task loadProducts()
    {
      products = await sync.GetAllProducts();
    }

    private async Task loadOrders() {
      orders = await sync.GetUserOrders(user_id);
    }


    private void TabChanged(object sender, SelectionChangedEventArgs e)
    {
      if (main.IsSelected)
      {
        //orderProds.Items.Clear();
      }
    }

    public void RefreshAllProds()
    {

      allPrds.Items.Clear();
      foreach (product prod in products)
      {
        allPrds.Items.Add(prod);
      }

      allPrds.Items.Refresh();
    }

    public async void refershAll()
    {
      await loadUser();
      await loadProducts();
      await loadOrders();
      RefreshAllProds();
      var viewOrdrs = new List<ViewOrder>();
      foreach (order ordr in orders)
      {
        if (!ordr.hidden || showHidden) viewOrdrs.Add(new ViewOrder { datetime = ordr.datetime, hidden = ordr.hidden, id = ordr.id, ordered = ordr.ordered, user_id = ordr.user_id });
      }
      myList.ItemsSource = viewOrdrs;
      myList.Items.Refresh();
      var user = currentUser;
      name.Text = user.name;
      surname.Text = user.surname;
      birth.Text = user.birthdate;
      usrame.Text = user.username;      
    }

    private void edit_Click(object sender, MouseButtonEventArgs e)
    {
      RefreshAllProds();
      var selected = myList.SelectedItem as ViewOrder;
      if (selected == null) return;

      App.Current.MainWindow.Content = new Order(selected, user_id);

    }
    private void del_Click(object sender, RoutedEventArgs e)
    {
      var selected = myList.SelectedItem as ViewOrder;
      if (myList.SelectedItem == null || selected.ordered) return;

      sync.DeleteOrder(new List<int> { selected.id });
      refershAll();
    }
    private void new_Click(object sender, RoutedEventArgs e)
    {
      sync.SaveOrder(new List<Models.order> { new order { user_id = user_id, datetime = DateTime.Now.ToPHPDatetime() } });
      refershAll();
    }

    private void hide_Click(object sender, RoutedEventArgs e)
    {
      var selected = myList.SelectedItem as ViewOrder;
      if (myList.SelectedItem == null) return;

      selected.hidden = !selected.hidden;

      sync.UpdateOrder(new List<order> { new order { datetime = selected.datetime, user_id = selected.user_id, hidden = selected.hidden, id = selected.id, ordered = selected.ordered } });

      refershAll();
    }

    private void order_Click(object sender, RoutedEventArgs e)
    {
      var selected = myList.SelectedItem as ViewOrder;
      if (myList.SelectedItem == null) return;

      selected.ordered = true;

      sync.UpdateOrder(new List<order> { new order { datetime = selected.datetime, user_id = selected.user_id, hidden = selected.hidden, id = selected.id, ordered = selected.ordered } });

      refershAll();
    }

    private void delProd_Click(object sender, RoutedEventArgs e)
    {
      var selected = allPrds.SelectedItem as product;
      if (allPrds.SelectedItem == null) return;

      sync.DeleteProduct(new List<int> { selected.id });

      refershAll();
    }

    private void addProd_Click(object sender, RoutedEventArgs e)
    {
      var name = prodName.Text;
      var price = 0;
      int.TryParse(prodPrice.Text, out price);
      var desc = prodDesc.Text;
      if (price == 0 || string.IsNullOrWhiteSpace(name)) return;
      sync.SaveProduct(new List<product> { new product { name = name, price = price, description = desc } });

      refershAll();
    }

    private void saveUsr_Click(object sender, RoutedEventArgs e)
    {
      var nm = name.Text;
      var srnm = surname.Text;
      var brth = user.ToBirthdayFormat(Convert.ToDateTime(birth.Text));
      var usrnm = usrame.Text;
      if (string.IsNullOrWhiteSpace(usrnm)) return;
      var usr = currentUser;
      usr.name = nm;
      usr.surname = srnm;
      usr.birthdate = brth;
      usr.username = usrnm;
      sync.UpdateUser(new List<user> { usr });
      refershAll();
    }

    private void savePass_Click(object sender, RoutedEventArgs e)
    {
      var ups = pass.Password.EncodeSHA256();
      var rep = repPass.Password.EncodeSHA256();
      if (ups != rep && !string.IsNullOrWhiteSpace(ups)) return;
      var usr = currentUser;
      usr.password = ups;
      sync.UpdateUser(new List<user> { usr });
      pass.Password = "";
      repPass.Password = "";
    }

    private void sHide_Click(object sender, RoutedEventArgs e)
    {
      showHidden = !showHidden;
      sHide.Content = showHidden ? "hide hidden" : "show hidden";
      refershAll();
    }

    private void logout_Click(object sender, RoutedEventArgs e)
    {
      App.Current.MainWindow.Content = new LoginPage();
    }

    private void sync_Click(object sender, RoutedEventArgs e)
    {
      SyncIt();
    }
    private async void SyncIt()
    {
      await Task.Run(() => sync.SyncAll());
    }
  }
}
