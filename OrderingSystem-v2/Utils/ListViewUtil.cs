using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OrderingSystem_v2.Models;
using OrderingSystem_v2.WebClient;
using OrderingSystem_v2.Utils;

namespace OrderingSystem_v2.Utils
{
  public static class ListViewUtil
  {
    public static void manageSwicthClick<T>(ListView list1, ListView list2, bool invert)
    {
      var item = (T)Convert.ChangeType((invert) ? list2.SelectedItem : list1.SelectedItem, typeof(T));
      if (item != null)
      {
        if (!invert)
        {
          list2.Items.Add(item);
          list1.Items.Remove(item);
        }
        else
        {
          list2.Items.Remove(item);
          list1.Items.Add(item);
        }
        list1.Items.Refresh();
        list2.Items.Refresh();
      }
    }
  }
}
